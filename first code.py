import sys 
import numpy as np

class City:
    def __init__(self,dx,dy):
        self.dx = dx
        self.dy = dy
        self.City = np.zeros(shape=(dx,dy),dtype=int)
        
    def takevalues(self,n_shops):
        return  zip(*[(map(int, values[i].split())) for i in range(1,n_shops+1)])
